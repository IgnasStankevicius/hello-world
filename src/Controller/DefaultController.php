<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    /**
     * TODO: Add the double asterisk in the line above for this route to work
     * @Route("/", name="home")
     */
    public function index(Request $request)
    {

        function microtime_float()
        {
            list($usec, $sec) = explode(" ", microtime());
            return ((float)$usec + (float)$sec);
        }

        //test1//

        $time_start = microtime_float();

        // Sleep for a while
        for ($i=0; $i<1000; $i++) {
            $file = fopen(__DIR__, "r");
        };

        $time_end = microtime_float();
        $time = $time_end - $time_start;

        $one = "Test 1: $time seconds\n";

        echo $one . "<br>";

        //test2//

        $time_start = microtime_float();

        // Sleep for a while
        for ($i=0; $i<3; $i++) {
            $file = file_get_contents("http://www.google.com");
        };

        $time_end = microtime_float();
        $time = $time_end - $time_start;


        $two = "Test 2: $time seconds\n";

        echo $two  . "<br>";

        //test3

        $time_start = microtime_float();

        // Sleep for a while
        for ($i=0; $i<10; $i++) {
            $a = array_fill(0, 1000, mt_rand(0, 1000));
            $b = array_fill(0, 1000, mt_rand(0, 1000));
            $c = [];
            foreach ($a as $h) {
                foreach ($b as $j) {
                    $c[] = $h*$j;
                }
            }
        };

        $time_end = microtime_float();
        $time = $time_end - $time_start;

        $three = "Test 3: $time seconds\n";

        echo $three  . "<br>";

        $file = "../result.txt";
        $cont = $one . $two . $three;
        file_put_contents($file, $cont);

        return new Response("hello world");
    }
}
